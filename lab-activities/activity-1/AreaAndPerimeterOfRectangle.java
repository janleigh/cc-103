/**
 * @(#)AreaAndPerimeterOfRectangle.java
 * Write a program that will compute and display
 * the area and perimeter of a rectangle.
 * Formula is:
 * Area = length * width
 * Perimeter = 2 * (length + width)
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/10
 */

import java.util.Scanner;

class AreaAndPerimeterOfRectangle {
        
    /**
     * @param {String[]} [args] the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char choice;

        do {
            System.out.print("Enter the length of the rectangle: ");
            double length = scanner.nextDouble();

            System.out.print("Enter the width of the rectangle: ");
            double width = scanner.nextDouble();

            double area = length * width;
            double perimeter = 2 * (length + width);

            System.out.println("\n\nThe area of the rectangle is " + area + ".");
            System.out.println("The perimeter of the rectangle is " + perimeter + ".");

            System.out.print("Do you want to calculate again? (y/n): ");
            choice = Character.toLowerCase(scanner.next().charAt(0));
        } while (choice == 'y');

        scanner.close(); // To fix resource leak.
    }
}
