/**
 * @(#)DollarToPeso.java
 * Write a program that would input a value in dollar
 * then convert and display the peso equivalent.
 * Presume that one dollar is equivalent to 55.75 pesos.
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/10
 */

import java.util.Scanner;

class DollarToPeso {

    /**
     * @param {String[]} [args] the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the amount in dollars: ");
        double dollars = scanner.nextDouble();

        double conversionRate = 55.75;
        double pesos = dollars * conversionRate;

        System.out.println("\n\n" + dollars + " dollars is equal to " + pesos + " pesos.");

        scanner.close(); // To fix resource leak.
    }

}