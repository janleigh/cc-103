/**
 * @(#)GradeOfStudent.java
 * Write a program that will compute the grade of the student
 * based on his scores as follows:
 * Prelim - 20%
 * Midterm - 25%
 * Pre-Finals - 25%
 * Finals - 30%
 * 
 * Input their scores for prelim, midterm, prefinal, and final,
 * then compute and display their grade.
 * NOTE: Score is between 1 and 100.
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/10
 */

import java.util.Scanner;

class GradeOfStudent {

    /**
     * Validates the score of a student.
     * 
     * @param {int} [score] the score to be validated
     * @return the validated score
     */
    static int validateScore(int score) {
        if (score < 1 || score > 100) {
            System.out.println("Invalid score. Please enter a score between 1 and 100.");
            System.exit(0);
        }

        // haha tinamad
        return score;
    }

    /**
     * @param {String[]} [args] the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your prelim score: ");
        int prelim = validateScore(scanner.nextInt());

        System.out.print("Enter your midterm score: ");
        int midterm = validateScore(scanner.nextInt());

        System.out.print("Enter your pre-finals score: ");
        int preFinals = validateScore(scanner.nextInt());

        System.out.print("Enter your finals score: ");
        int finals = validateScore(scanner.nextInt());

        double grade = (prelim * 0.2) + (midterm * 0.25) + (preFinals * 0.25) + (finals * 0.3);
        System.out.println("\n\nYour grade is " + grade + ".");

        scanner.close(); // To fix resource leak.
    }
}
