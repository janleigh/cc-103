/**
 * @(#)GrossPay.java
 * Write a program to calculate the gross pay
 * of a worker where total hours worked and
 * rate per hour is the input value.
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/10
 */

import java.util.Scanner;
import java.text.DecimalFormat;

class GrossPay {
        
    /**
     * @param {String[]} [args] the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.00");

        System.out.print("Enter the number of hours worked: ");
        double hoursWorked = scanner.nextDouble();

        System.out.print("Enter the rate per hour: ");
        double ratePerHour = scanner.nextDouble();

        double grossPay = hoursWorked * ratePerHour;
        System.out.println("\n\nThe gross pay is " + df.format(grossPay));

        scanner.close(); // To fix resource leak.
    }
}
