/**
 * @(#)SwapTwoNumbers.java
 * Write a program that will accept two numbers
 * and then swap the arrangement of the assigned value.
 * Display the new values.
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/10
 */

import java.util.Scanner;

class SwapTwoNumbers {
        
    /**
     * @param {String[]} [args] the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the first number: ");
        int firstNumber = scanner.nextInt();

        System.out.print("Enter the second number: ");
        int secondNumber = scanner.nextInt();

        System.out.println("\n\nBefore swapping:");
        System.out.println("First number: " + firstNumber);
        System.out.println("Second number: " + secondNumber);

        int temp = firstNumber;
        firstNumber = secondNumber;
        secondNumber = temp;

        System.out.println("\nAfter swapping:");
        System.out.println("First number: " + firstNumber);
        System.out.println("Second number: " + secondNumber);

        scanner.close(); // To fix resource leak.
    }
}
