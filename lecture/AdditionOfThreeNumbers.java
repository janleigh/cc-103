/**
 * @(#)AdditionOfThreeNumbers.java
 *
 * Write a program in Java that will ask the user to input three numbers and then
 * the program will find the total sum of three numbers given by the user.
 *
 * @author Mu�oz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/5
 */

import java.util.Scanner;

public class AdditionOfThreeNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println();
        System.out.println("===== ADDITION OF THREE NUMBERS =====");
        System.out.println();
        
        System.out.print("Enter first number: ");
        int a = scanner.nextInt();
        
        System.out.print("Enter second number: ");
        int b = scanner.nextInt();
        
        System.out.print("Enter third number: ");
        int c = scanner.nextInt();
        
        int sum = a + b + c;
        
        System.out.println();
        System.out.print("The sum of " + a + ", " + b + ", and " + c + " is " + sum + ".");
        System.out.println("\n\n");
        System.out.println("===== END OF PROGRAM =====");
        System.out.println();

        scanner.close();
    }
}
