/**
 * @(#)AreaOfTheCircle.java
 *
 *
 * @author Mu�oz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/5
 */

import java.util.Scanner;
import java.text.DecimalFormat;

public class AreaOfTheCircle {
	
	public static double solveArea(double r) {
		return Math.PI * r * r;
	}
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	Scanner scanner = new Scanner(System.in);
    	DecimalFormat df = new DecimalFormat("###.##");
    	
    	System.out.println();
        System.out.println("===== AREA OF THE CIRCLE SOLVER =====");
        System.out.println();
        
        System.out.print("Enter the radius of the circle: ");
        double radius = scanner.nextDouble();
        System.out.println();
        
        System.out.print("The given radius is " + df.format(radius) + ". The area of the circle is " + df.format(solveArea(radius)));
        System.out.println("\n\n");
        System.out.println("===== END OF PROGRAM =====");
        System.out.println();        

        scanner.close();
    }
}
