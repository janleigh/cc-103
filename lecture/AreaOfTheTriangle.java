/**
 * @(#)AreaOfTheTriangle.java
 *
 *
 * @author Mu�oz, Jan Leigh Augustine S.
 * @version 1.00 2024/2/7
 */

import java.util.Scanner;

public class AreaOfTheTriangle {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        char ch;
        double triangleBase = 0;
        double triangleHeight = 0;
        double solveArea = 0;

        do {
            System.out.println();
            System.out.println("\tAREA OF TRIANGLE SOLVER");
            System.out.println();

            System.out.print("What is the length of the base of the triangle: ");
            triangleBase = input.nextDouble();

            System.out.print("What is the length of the height of the triangle: ");
            triangleHeight = input.nextDouble();

            solveArea = (triangleBase * triangleHeight) / 2;

            System.out.println();
            System.out.println("The area of the triangle is: " + solveArea);
            System.out.println();
            System.out.print("\nDo you want to continue? (Type Y/N): ");

            ch = input.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');

        System.out.println();
        System.out.println("\tTHANK YOU FOR USING THIS PROGRAM");
        System.out.println("\n\n");

        input.close();
    }
}
