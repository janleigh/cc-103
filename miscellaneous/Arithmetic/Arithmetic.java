/**
 * @(#)Arithmetic.java
 * Create a program that would input two-integer-values
 * then compute and display the sum, product, quotient,
 * difference and remainder.
 *
 * @author Muñoz, Jan Leigh Augustine S.
 * @version 1.00 2024/1/29
 */

// Imports all classes from java.util package (e.g Scanner, HashMap, Date, Random).
import java.util.*;

public class Arithmetic {

    // inefficient. commented as its only used twice inside the main method.
    // static Scanner con = new Scanner(System.in);

    public static void main(String[] args) {
        // Creates an Scanner object named con.
        // BREAKDOWN:
        // Scanner = class name.
        // con = object name.
        // new = creates a new object.
        // Scanner(System.in) = Scanner constructor with an argument of System.in where
        //                      System.in is an InputStream object that reads from the
        //                      keyboard.
        Scanner con = new Scanner(System.in);

        // alam nyo na to ._.
        int num1, num2, prod, diff, quot, sum, rem;

        // no need for this line as the variables are already initialized to 0.
        // num1 = num2 = prod = diff = quot = sum = rem = 0;

        // BREAKDOWN:
        // 1. System.out.print("Enter first value: ");
        // This part uses the System.out.print method to display a message to the console.
        // The message prompts the user to enter the first value.
        // Unlike System.out.println, System.out.print does not append a newline character at the end,
        // so any subsequent output will appear on the same line.
        // ======================================================
        // 2. num1 = con.nextInt();
        // This part uses the nextInt method of the Scanner object con to read an integer input from the user.
        // The inputted value is then assigned to the num1 variable.
        System.out.print("Enter first value: ");
        num1 = con.nextInt();

        // same as above.
        System.out.print("Enter second value: ");
        num2 = con.nextInt();

        // calculates the product, difference, quotient, sum, and remainder of num1 and num2.
        prod = num1 * num2;
        diff = num1 - num2;
        quot = num1 / num2;
        sum = num1 + num2;
        rem = num1 % num2;

        // BREAKDOWN:
        // 1.
        // System.out.println() is a Java method used to print text to the console.
        // The text is followed by a newline, so the next output will be printed on a new line.
        // 2.
        // The argument to System.out.println is a string that includes several variables (prod, diff, quot, sum, rem).
        // These variables are concatenated with string literals using the + operator.
        // The string literals include descriptive text and newline characters (\n) to format the output.
        // 3.
        // The variables prod, diff, quot, sum, rem are expected to hold the results of arithmetic operations performed earlier in the code.
        // Specifically, prod is the product, diff is the difference, quot is the quotient, sum is the sum, and rem is the remainder of two numbers.
        System.out.println(
            "\nProduct: " +
            prod +
            "\nDifference: " +
            diff +
            "\nQuotient: " +
            quot +
            "\nSum: " +
            sum +
            "\nRemainder: " +
            rem
        );

        con.close();
    }
}
